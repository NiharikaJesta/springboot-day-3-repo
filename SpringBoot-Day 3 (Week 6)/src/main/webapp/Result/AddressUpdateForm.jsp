<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri = "http://www.springframework.org/tags/form"
 prefix = "form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
   
      <h2>Enter Employee  Information</h2>
      <form:form method = "POST" action = "updateContactList" modelAttribute="updateobj">
         <table>
               
                <tr>
               <td><form:label path = "addressId">AddressId</form:label></td>
               <td><form:input path = "addressId" /></td>
            </tr>
           
             <tr>
               <td><form:label path = "flatNumber">FlatNumber</form:label></td>
               <td><form:input path = "flatNumber" /></td>
            </tr>
           
             <tr>
               <td><form:label path = "area">Area</form:label></td>
               <td><form:input path = "area" /></td>
            </tr>
           
             <tr>
               <td><form:label path = "city">City</form:label></td>
               <td><form:input path = "city" /></td>
            </tr>
           
           
             <tr>
               <td><form:label path = "zipcode">zipcode</form:label></td>
               <td><form:input path = "zipcode" /></td>
            </tr>
           
             
                <tr>
                    <td align="center" colspan="2"><input type="update"
                        value="Save"></td>
                </tr>
                
            </table>
        </form:form>
  
</body>
</html>