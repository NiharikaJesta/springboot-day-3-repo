<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<title>Address Form</title>
</head>
<body>
    <div align="center">
         <form:form method = "POST" action = "saveAddress" modelAttribute="address">
            <table>
               
                <tr>
               <td><form:label path = "addressId">AddressId</form:label></td>
               <td><form:input path = "addressId" /></td>
            </tr>
           
             <tr>
               <td><form:label path = "flatNumber">FlatNumber</form:label></td>
               <td><form:input path = "flatNumber" /></td>
            </tr>
           
             <tr>
               <td><form:label path = "area">Area</form:label></td>
               <td><form:input path = "area" /></td>
            </tr>
           
             <tr>
               <td><form:label path = "city">City</form:label></td>
               <td><form:input path = "city" /></td>
            </tr>
           
           
             <tr>
               <td><form:label path = "zipcode">zipcode</form:label></td>
               <td><form:input path = "zipcode" /></td>
            </tr>
           
             
                <tr>
                    <td align="center" colspan="2"><input type="submit"
                        value="Save"></td>
                </tr>
                
            </table>
        </form:form>
      </div>
   
</body>
</html>








