<%@taglib uri = "http://www.springframework.org/tags/form"
 prefix = "form"%>

<html>
   <head>
      <title>Spring MVC Form Handling</title>
   </head>

   <body>
   <div align="center">
  <h1> <form color=Red>***Registration  page***</form></h1>
      <form:form method = "POST" action = "Register" modelAttribute="registerData">
         <table>
            <tr>
               <td><form:label path = "userId">UserId</form:label></td>
               <td><form:input path = "userId" /></td>
            </tr>
            <tr>
               <td><form:label path = "userName">UserName</form:label></td>
               <td><form:input path = "userName" /></td>
            </tr>
            <tr>
               <td><form:label path = "password">Password</form:label></td>
               <td><form:input path = "password" /></td>
            </tr>
            <tr>
               <td><form:label path = "emailId">EmailId</form:label></td>
               <td><form:input path = "emailId" /></td>
            </tr>
            <tr>
               <td colspan = "2">
                  <input type = "submit" value = "Register"/>
               </td>
            </tr>
         </table>  
      </form:form>
    </div>
   </body>
   
</html>