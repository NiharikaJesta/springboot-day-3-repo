<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Contact Manager Home</title>
</head>
<body>
	<div align="center">
		<h1>Contact List</h1>
		<h3>
			<a href="contactForm">New Contact</a>
		</h3>
		<table border="1">
			<th>contactId</th>
			<th>firstName</th>
			<th>lastName</th>
			<th>mobileNumber</th>
			<th>officeNumber</th>
			<th>emailId</th>
			<th>city</th>
			<th>Action</th>

			<c:forEach var="contact" items="${contactList}" varStatus="status">
				<tr>

					<td>${contact.contactId}</td>
					<td>${contact.firstName}</td>
					<td>${contact.lastName}</td>
					<td>${contact.mobileNumber}</td>
					<td>${contact.officeNumber}</td>
					<td>${contact.emailId}</td>
					<td>${contact.city}</td>
					<!--   <td>
                        <a href="updatecontact${contact.contactId}">Edit</a>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="deleteContact?contactId=${contact.contactId}">Delete</a>
                    </td>  -->


					<td><a href="updatecontact/${contact.contactId}">Edit</a>
					 &nbsp;&nbsp;&nbsp;&nbsp;
					 <a href="deleteContact/${contact.contactId}"
							onclick="return confirm('Do you want to delete? ')">Delete</a></td>
				</tr>
				
			</c:forEach>
		</table>
	</div>
</body>   
</html>