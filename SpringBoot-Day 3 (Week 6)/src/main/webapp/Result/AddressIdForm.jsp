<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<div align="center">
	 <h2>Enter Contact Id to update the  Information</h2>
      <form:form method = "POST" action = "updateAddress" modelAttribute="addIdData">
         <table>
            <tr>
               <td><form:label path = "contactId">contactId</form:label></td>
               <td><form:input path = "contactId" /></td>
            </tr>
           
            <tr>
               <td colspan = "2">
                  <input type = "submit" value = "Update"/>
               </td>
            </tr>
         </table>  
      </form:form>
      </div>
</body>
</html>