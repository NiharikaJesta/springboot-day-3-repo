<%@taglib uri = "http://www.springframework.org/tags/form"
 prefix = "form"%>

<html>
   <head>
      <title>Spring MVC Form Handling</title>
   </head>

   <body>
   <div align="center">
   <form color=blue>***login page***</form>
   
      <h2>Login Page</h2>
      <form:form method = "POST" action = "validatelogin" modelAttribute="logindata">
        <table>
            <tr>
               <td><form:label path = "userName">userName</form:label></td>
               <td><form:input path = "userName" /></td>
            </tr>
            
             <tr>
               <td><form:label path = "password">password</form:label></td>
               <td><form:input path = "password" /></td>
            </tr>
           
            <tr>
               <td colspan = "2">
                  <input type = "submit" value = "Login"/>
               </td>
            </tr>
         </table>  
      </form:form>
      <h2>${LoginErrorMessage}</h2>
      </div>
   </body>
   
</html>
