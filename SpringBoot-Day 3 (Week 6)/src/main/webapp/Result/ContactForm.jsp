<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<title>Contact Form</title>
</head>
<body>
	<div align="center">
		<form:form method="POST" action="saveContact" modelAttribute="contact">
			<table>

				<tr>
					<td>Contact Id</td>
					<td><form:input type="text" path="contactId" /></td>
				</tr>
				<tr>
					<td>first name</td>
					<td><form:input type="text" path="firstName" /></td>
				</tr>
				<tr>
					<td>last name</td>
					<td><form:input type="text" path="lastName" /></td>
				</tr>
				<tr>
					<td>mobileNumber</td>
					<td><form:input type="text" path="mobileNumber" /></td>
				</tr>
				<tr>
					<td>officeNumber</td>
					<td><form:input type="text" path="officeNumber" /></td>
				</tr>
				<tr>
					<td>emailId</td>
					<td><form:input type="text" path="emailId" /></td>
				</tr>
				<tr>
					<td>city</td>
					<td><form:input type="text" path="city" /></td>
				</tr>

				<tr>
					UserId:
					<form:select path="userObj">
						<form:options items="${contact.userList}" />

					</form:select>

				</tr>


				<tr>
					<td align="center" colspan="2"><input type="submit"
						value="Update & Save"></td>
				</tr>

			</table>
		</form:form>
	</div>

</body>
</html>