<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri = "http://www.springframework.org/tags/form"
 prefix = "form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
   
      <h2>Update Form Of Contact</h2>
      <form:form method = "POST" action = "/update" modelAttribute="updateobj">
         <table>
           <tr>
                    <td>Contact Id</td>
                    <td><form:input type="text" path="contactId" /></td>
                </tr>
                <tr>
                    <td>first name</td>
                    <td><form:input type="text" path="firstName" /></td>
                </tr>
                <tr>
                    <td>last name</td>
                    <td><form:input type="text" path="lastName" /></td>
                </tr>
                <tr>
                    <td> mobileNumber</td>
                    <td><form:input type="text" path="mobileNumber" /></td>
                </tr>
                <tr>
                    <td>officeNumber</td>
                    <td><form:input type="text" path="officeNumber" /></td>
                </tr>
                <tr>
                    <td>emailId</td>
                    <td><form:input type="text" path="emailId" /></td>
                </tr>
                 <tr>
                    <td>city</td>
                    <td><form:input type="text" path="city" /></td>
                </tr>
                <tr>
                    <td align="center" colspan="2"><input type="submit" value="Update"/></td>
                </tr>
               
         </table>  
      </form:form>
  
</body>
</html>