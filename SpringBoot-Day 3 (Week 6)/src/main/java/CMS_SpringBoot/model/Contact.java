package CMS_SpringBoot.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;

import java.util.List;

import javax.persistence.*;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table
public class Contact {
	@Id
	@Column
	private int contactId;
	@Column(name="firstname")
	private String firstName;
	@Column(name="lastname")
	private String lastName;
	@Column(name="mobilenumber")
	private String mobileNumber;
	private String officeNumber;
	private String emailId;
	private String city;
	// private int userId; //FK

	@ManyToOne
	//@JoinColumn(name = "userid", referencedColumnName = "userId")
	private User userObj;

	public int getContactId() {
		return contactId;
	}

	public void setContactId(int contactId) {
		this.contactId = contactId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getOfficeNumber() {
		return officeNumber;
	}

	public void setOfficeNumber(String officeNumber) {
		this.officeNumber = officeNumber;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public User getUserObj() {
		return userObj;
	}

	public void setUserObj(User userObj) {
		this.userObj = userObj;
	}
	
//	@ManyToOne
//	private Contact cObj;
//
//	public Contact getcObj() {
//		return cObj;
//	}
//
//	public void setcObj(Contact cObj) {
//		this.cObj = cObj;
//	}
	
	@Transient
	private List<Integer> userList;

	public List<Integer> getUserList() {
		return userList;
	}

	public void setUserList(List<Integer> userList) {
		this.userList = userList;
	}
	
}
