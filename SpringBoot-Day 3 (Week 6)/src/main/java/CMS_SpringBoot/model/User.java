package CMS_SpringBoot.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.sun.istack.NotNull;

@Entity
@Table
public class User {
	@Id
	@NotNull
	@GeneratedValue(strategy = GenerationType.IDENTITY)// (Auto incremented)
	private int userId;
	private String userName;
	private String password;
	private String emailId;

	@OneToMany
//@JoinColumn(name = "userId")
//@OneToMany(cascade=CascadeType.ALL,targetEntity = Contact.class)
//@JoinColumn(name="user_id",referencedColumnName = "userId")
	private List<Contact> contactList;

	public int getUserId() {
		return userId;
	}
 
	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public List<Contact> getContactList() {
		return contactList;
	}

	public void setContactList(List<Contact> contactList) {
		this.contactList = contactList;
	}

}
