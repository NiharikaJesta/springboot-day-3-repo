package CMS_SpringBoot.Configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;


@Configuration
@EnableWebSecurity
public class ContactConfiguration extends WebSecurityConfigurerAdapter {

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/resources/**");

	}

	//http://localhost:8080/
	// it is accessible by all users
	@Override
	public void configure(HttpSecurity http) throws Exception {

		http.authorizeRequests().antMatchers("/").permitAll().antMatchers("/ListOfContacts")
				.hasAnyRole("USER", "ADMIN")
				.anyRequest().authenticated().and().formLogin().loginPage("/Securitylogin").permitAll().and().logout()
				.permitAll();

		http.csrf().disable();
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder authenticationmanager) throws Exception {
		authenticationmanager.inMemoryAuthentication().withUser("admin").password("{noop}admin")
				.authorities("ROLE_ADMIN").and().withUser("user").password("{noop}user")
				.authorities("ROLE_USER", "ROLE_ADMIN");

	}
}
