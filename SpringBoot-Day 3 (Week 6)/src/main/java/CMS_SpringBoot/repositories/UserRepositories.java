package CMS_SpringBoot.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import CMS_SpringBoot.model.User;
@Repository
public interface UserRepositories extends JpaRepository<User,Integer> {
	@Query("select u from User u where userName=?1 and password=?2")
	public Optional<User> userValidation(String u,String p);
	
	//to get user id list........used for drop down list while entering contact form
	@Query("select u.userId from User u")
	public List<Integer>  getuserlist();
	
	
}
