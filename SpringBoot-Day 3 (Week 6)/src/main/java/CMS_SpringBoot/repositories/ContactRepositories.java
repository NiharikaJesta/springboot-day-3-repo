package CMS_SpringBoot.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import CMS_SpringBoot.model.Contact;


@Repository
public interface ContactRepositories extends JpaRepository<Contact,Integer>{
	@Query("select cont from Contact cont")
	public List<Contact> getContactList();
	
	@Query("select cont from Contact cont order by firstname Asc")
	public List<Contact> getNameOrderByAsc();
	
	@Query("select cont from Contact cont order by firstname,lastName Asc")
	public List<Contact> getNameOrderByFirstLastAsc();
	
	//Display the contact details whose number starts with “7777”
	@Query("select c from Contact c where c.contactId like '1%'")
	public List<Contact> getContactDetailsStartsWithId(int id1);
	
	//Dispaly all the contacts which name contains with "Ajay"
	@Query("select c from Contact c where c.firstName like %?1%")
	public List<Contact> getcontactwhichNamecontainswith(char name);
	
	//Dispaly all the contacts which name contains with "Ajay" and from "delhi".
	@Query("select c from Contact c where c.firstName=?1  and c.city=?2")
	public List<Contact> getcontactwhichNamecontainsWithandFrom(String name,String city);
	
	
	
	
	
	
//	@Modify
//	@Query("d")
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//to get contact id list........used for drop down list
//	@Query("select c.contactId from Contact c")
//	public List<Integer> getContactListForContactId();
	
}
