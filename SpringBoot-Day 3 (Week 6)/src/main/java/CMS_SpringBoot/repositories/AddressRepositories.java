package CMS_SpringBoot.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import CMS_SpringBoot.model.Address;

@Repository
public interface AddressRepositories extends JpaRepository<Address,Integer> {

}
