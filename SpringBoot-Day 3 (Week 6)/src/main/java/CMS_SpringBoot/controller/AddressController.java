package CMS_SpringBoot.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import CMS_SpringBoot.model.Address;
import CMS_SpringBoot.model.Contact;
import CMS_SpringBoot.repositories.AddressRepositories;

@Controller
public class AddressController {
	@Autowired
	AddressRepositories addRep;
	
	@RequestMapping("/addressForm")
	public ModelAndView getAddressForm()
	{
		return new ModelAndView("AddressForm","address",new Address());
	}
	
	@RequestMapping("/saveAddress")
	public String saveAddress(@ModelAttribute("address") Address add)
	{
		addRep.save(add);
		return "success";
	}
	
	@RequestMapping("/updateAddressById")
	public ModelAndView getFormIdForAddress() {

		return new ModelAndView("updateIdFormAddress", "addIdData", new Address());

	}

	@RequestMapping("/updateAddress")
	public ModelAndView updateAddress(@ModelAttribute("addIdData") Address add) {
		
		Optional<Address> aObj=addRep.findById(add.getAddressId());
		
		Address afind = null;
		if (aObj.isPresent()) {
			afind = aObj.get();
		}
		return new ModelAndView("AddressUpdateForm", "updateobj", afind);

	}
	@RequestMapping("/updateAddressList")
	public ModelAndView getupdatedAddress(@ModelAttribute("updateobj") Address updatedAdd) {

		addRep.save(updatedAdd);
        return new ModelAndView("success");

	}
}
