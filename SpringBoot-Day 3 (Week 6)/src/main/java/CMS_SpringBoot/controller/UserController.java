package CMS_SpringBoot.controller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import CMS_SpringBoot.model.Contact;
import CMS_SpringBoot.model.User;
import CMS_SpringBoot.repositories.ContactRepositories;
import CMS_SpringBoot.repositories.UserRepositories;

@Controller
public class UserController {
	@Autowired
	UserRepositories userRep;
	@Autowired
	ContactRepositories conRep;
	@GetMapping("/")
	public String getWelcomePage() {
		return "welcome";
	}

	@RequestMapping("/login")
	public  ModelAndView  login(@ModelAttribute("LoginErrorMessage") String errorMessage)
	{
		ModelAndView ModelAndView= new ModelAndView("LoginPage","logindata",new User());
		if(errorMessage==null)
		{
			return ModelAndView;
		} 
		else
		{
			ModelAndView.addObject("LoginErrorMessage",errorMessage);
			return ModelAndView;
		}
		
	}
	
	
	
	@RequestMapping("/validatelogin")
	public ModelAndView validateUser(@ModelAttribute("logindata") User u,HttpSession session) {
		Optional<User> uList=userRep.userValidation(u.getUserName(),u.getPassword());
		User ufind=null;
		if(uList.isPresent())
		{
			System.out.println("User found");
			ufind=uList.get();
			return new ModelAndView("redirect:/DashBoard");
		}
		else
		{
			System.out.println("User not found");
			return new ModelAndView("LoginPage","LoginErrorMessage","Invalid username or password");
		}
	}

	
	@GetMapping("/registration")
	public ModelAndView getRegistration_Form() {
		
		return new ModelAndView("RegisterPage","registerData",new User());
	}
	@PostMapping("/Register")
	public ModelAndView saveRegistrtionData(@ModelAttribute("registerData")User u) {
		userRep.save(u);
		return new ModelAndView("redirect:/login");	
	}
	
	@RequestMapping("/logout")
	public String logout(HttpSession session) {
		session.invalidate();
		return "redirect:/login";
	}

	@RequestMapping(value = "/Securitylogin", method = RequestMethod.GET)
	public ModelAndView login(String error, String logout) {
		System.out.println("Coming here ");
		if (error != null)
			return new ModelAndView("SecurityLogin", "Errormsg", "Your username and password are invalid.");

		if (logout != null)
			return new ModelAndView("SecurityLogin", "msg", "You have been logged out successfully.");
		return new ModelAndView("SecurityLogin");
	} 
}
 
