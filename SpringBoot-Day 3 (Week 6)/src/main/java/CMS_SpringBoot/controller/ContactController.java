package CMS_SpringBoot.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import CMS_SpringBoot.model.Contact;
import CMS_SpringBoot.model.User;
import CMS_SpringBoot.repositories.ContactRepositories;
import CMS_SpringBoot.repositories.UserRepositories;
import CMS_SpringBoot.service.ContactService;

@Controller
public class ContactController {

	@Autowired
	ContactRepositories conRep;

	@Autowired
	UserRepositories uRep;

	@Autowired
	ContactService cRep;

	@RequestMapping("/DashBoard")
	public ModelAndView listOfContacts(ModelAndView model) {

		List<Contact> contactList = conRep.getContactList();
		model.addObject("contactList", contactList);
		model.setViewName("DashBoardPage");
		return model;
	}

	@RequestMapping("/contactForm")
	public ModelAndView getForm() {

		List<Integer> arrList = uRep.getuserlist();
		Contact c = new Contact();
		c.setUserList(arrList);
		if (arrList != null) {
			return new ModelAndView("ContactForm", "contact", c);

		} else {

			return new ModelAndView("ContactForm", "contact", new Contact());
		}
	}

	@RequestMapping("/saveContact")
	public String saveContact(@ModelAttribute("contact") Contact cont) {
		conRep.save(cont);
		return "redirect:/DashBoard";
	}

	@RequestMapping("/updatecontact/{contactId}")
	public ModelAndView updateContactForm(@PathVariable("contactId") int contactId) {
		System.out.println("update id found");
		Contact contact = conRep.getById(contactId);
		return new ModelAndView("UpdateForm", "updateobj", contact);

	}

	@PostMapping("/update")
	public ModelAndView updateContact(@ModelAttribute("updateobj") Contact c) {
		System.out.println("coming");
		List<Contact> contList = new ArrayList<Contact>();
		
		//c.getOfficeNumber();
		//c.getCity();
	//Contact updatedContact=conRep.getById(c.getContactId());
	//updatedContact.setFirstName(c.getFirstName());

	//	contList.add(updatedContact);
conRep.save(c);
		System.out.println("updated");
		
		return new ModelAndView("redirect:/DashBoard");
	}

	
	
	@RequestMapping("/deleteContact/{contactId}")
	public ModelAndView deleteContact(@PathVariable("contactId") int contactId) {
		System.out.println("deleted");
		Optional<Contact> cont = conRep.findById(contactId);
		if (cont != null) {

			Contact c = cont.get();
			c.setUserObj(null);
//			
//			User userid=c.getUserObj();
//			uRep.delete(userid);
			conRep.delete(c);
		}
		// conRep.deleteById(contactId);
		return new ModelAndView("redirect:/DashBoard");

	}
	

	/*
	 * @RequestMapping(value= "/deleteContact/{id}", method= RequestMethod.DELETE)
	 * public void deleteEmployeeById(@PathVariable int id) throws Exception {
	 * System.out.println(this.getClass().getSimpleName() +
	 * " - Delete employee by id is invoked."); Optional<Contact>
	 * c=conRep.findById(id);
	 * 
	 * if(!c.isPresent()) throw new Exception("Could not find employee with id- " +
	 * id); conRep.deleteById(id);
	 * 
	 * }
	 */

	// ...........update using form
//	@RequestMapping("/updateById")
//	public ModelAndView getFormForId() {
//
//		return new ModelAndView("updateIdForm", "contactIdData", new Contact());
//
//	}
//	@RequestMapping("/update")
//	public ModelAndView updateContact(@ModelAttribute("contactIdData") Contact cont) {
//
//		Optional<Contact> cObj = conRep.findById(cont.getContactId());
//
//		Contact cfind = null;
//		if (cObj.isPresent()) {
//			cfind = cObj.get();
//		}
//		return new ModelAndView("UpdateForm", "updateobj", cfind);
//
//	}
//	
//	@RequestMapping("/updateContactList")
//	public ModelAndView getupdatedContact(@ModelAttribute("updateobj") Contact updatedCont) {
//
//		conRep.save(updatedCont);
//		return new ModelAndView("redirect:/DashBoard");
//
//	}
//	
	// ............delete using form
//	@RequestMapping("/deleteById")
//	public ModelAndView deleteById() {
//
//		return new ModelAndView("deleteIdForm", "deleteData", new Contact());
//	}
//
//	@RequestMapping("/delete")
//	public ModelAndView deletedata(@ModelAttribute("deleteData") Contact contact) {
//		conRep.deleteById(contact.getContactId());
//		return new ModelAndView("redirect:/DashBoard");
//	}

	@RequestMapping("/getContactList")
	public String getContactData() {
		List<Contact> contactList = conRep.getContactList();
		if (contactList != null) {
			for (Contact c : contactList) {
				System.out.println("contactId    is :" + c.getContactId());
				System.out.println("firstname    is :" + c.getFirstName());
				System.out.println("lasttname    is :" + c.getLastName());
				System.out.println("mobilenumber is :" + c.getMobileNumber());
				System.out.println("officenumber is :" + c.getOfficeNumber());
				System.out.println("emailid      is :" + c.getEmailId());
				System.out.println("city         is :" + c.getCity());
			}
		}
		return "success";

	}

	@RequestMapping("/getNameOrderByAsc")
	public String getNameOrderByAsc() {
		List<Contact> contactList = conRep.getNameOrderByAsc();
		if (contactList != null) {
			for (Contact c : contactList) {

				System.out.println("firstname    is :" + c.getFirstName());
			}
		}
		return "success";
	}

	@RequestMapping("/getNameOrderByFLAsc")
	public String getNameOrderByFLAsc() {
		List<Contact> contactList = conRep.getNameOrderByFirstLastAsc();
		if (contactList != null) {
			for (Contact c : contactList) {

				System.out.println("firstname    is :" + c.getFirstName());
				System.out.println("lastname    is  :" + c.getLastName());
			}
		}
		return "success";
	}

	@RequestMapping("/getContactDetailsStartsWithId")
	public String getContactDetailsStartsWithId() {
		List<Contact> cList = conRep.getContactDetailsStartsWithId(1);
		if (cList != null) {
			for (Contact c : cList) {
				System.out.println("contactId    is :" + c.getContactId());
				System.out.println("firstname    is :" + c.getFirstName());
				System.out.println("lasttname    is :" + c.getLastName());
				System.out.println("mobilenumber is :" + c.getMobileNumber());
				System.out.println("officenumber is :" + c.getOfficeNumber());
				System.out.println("emailid      is :" + c.getEmailId());
				System.out.println("city         is :" + c.getCity());
				System.out.println("-----------------");
			}

		}
		return "success";
	}

	@RequestMapping("/getcontactwhichNamecontainswith")
	public String getcontactwhichNamecontainswith() {
		List<Contact> cList = conRep.getcontactwhichNamecontainswith('k');
		if (cList != null) {
			for (Contact c : cList) {
				System.out.println("contactId    is :" + c.getContactId());
				System.out.println("firstname    is :" + c.getFirstName());
				System.out.println("lasttname    is :" + c.getLastName());
				System.out.println("mobilenumber is :" + c.getMobileNumber());
				System.out.println("officenumber is :" + c.getOfficeNumber());
				System.out.println("emailid      is :" + c.getEmailId());
				System.out.println("city         is :" + c.getCity());
				System.out.println("-----------------");
			}

		}
		return "success";

	}

	@RequestMapping("/getcontactwhichNamecontainsWithandFrom")
	public String getcontactwhichNamecontainsWithandFrom() {
		List<Contact> cList = conRep.getcontactwhichNamecontainsWithandFrom("hari", "ramnagar");
		if (cList != null) {
			for (Contact c : cList) {
				System.out.println("contactId    is :" + c.getContactId());
				System.out.println("firstname    is :" + c.getFirstName());
				System.out.println("lasttname    is :" + c.getLastName());
				System.out.println("mobilenumber is :" + c.getMobileNumber());
				System.out.println("officenumber is :" + c.getOfficeNumber());
				System.out.println("emailid      is :" + c.getEmailId());
				System.out.println("city         is :" + c.getCity());
				System.out.println("-----------------");
			}

		}
		return "success";
	}
}

//	@Autowired
//	ContactService contactService;
//	
//	//creating a get mapping that retrieves all the contacts detail from the database   
//	@GetMapping("/contact")  
//	private List<Contact> getAllContacts()   
//	{  
//	return contactService.getAllContacts();  
//	} 
//	
//	@GetMapping("/contact/{contactid}")  
//	private Contact getBooks(@PathVariable("contactid") int cid)   
//	{  
//	return contactService.getContactsById(cid);
//	}
//	
//	//creating a delete mapping that deletes a specified book  
//	@DeleteMapping("/contact/{contactid}")  
//	private void deleteBook(@PathVariable("contactid") int cid)   
//	{  
//	contactService.delete(cid);  
//	}  
//	
//	//creating post mapping that post the book detail in the database  
//	@PostMapping("/contact")  
//	private int saveBook(@RequestBody  Contact contact)   
//	{  
//	contactService.saveOrUpdate(contact);  
//	return contact.getContactId() ;
//	}  
//	//creating put mapping that updates the book detail   
//	@PutMapping Contact update(@RequestBody Contact contact)   
//	{  
//	contactService.saveOrUpdate(contact);  
//	return contact;  
//	}  
