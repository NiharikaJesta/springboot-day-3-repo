package CMS_SpringBoot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import CMS_SpringBoot.model.*;
import CMS_SpringBoot.repositories.ContactRepositories;

import java.util.*;

@Service
public class ContactService {
	@Autowired
	ContactRepositories contactRep;

	// update
	public void updateContact( Contact contact) {
		List<Contact> contactList = getAllContacts();
		for (Contact c : contactList) {
			if (c.getContactId() == contact.getContactId()) {
				contactList.remove(c);
				break;
			}
		}
		contactList.add(contact);
		contactRep.saveAll(contactList);
	}
 

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	// getting all contacts record by using the method findaAll() of CrudRepository
	public List<Contact> getAllContacts() {
		List<Contact> contacts = new ArrayList<Contact>();
		contactRep.findAll().forEach(contacts1 -> contacts.add(contacts1));
		return contacts;
	}

	// getting a specific record by using the method findById() of CrudRepository
	public Contact getContactsById(int id) {
		return contactRep.findById(id).get();

	}

	// saving a specific record by using the method save() of CrudRepository
	public void saveOrUpdate(Contact contact) {
		contactRep.save(contact);

	}

	// deleting a specific record by using the method deleteById() of CrudRepository
	public void delete(int id) {
		contactRep.deleteById(id);

	}

	// updating a record
	public void update(Contact contacts, int contactId) {
		contactRep.save(contacts);

	}

}
